package banking;

/**
 * Created by prayagupd
 * on 4/15/15.
 */

public interface Observer {
  public void update(AccountManager accountManager);
}
