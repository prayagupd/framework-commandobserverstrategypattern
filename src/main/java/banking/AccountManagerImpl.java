package banking;

import banking.actions.Action;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by prayagupd
 * on 4/15/15.
 */

public class AccountManagerImpl implements AccountManager {
  private List<Observer> observers = new LinkedList<Observer>();
  private Action action;

  public void submitAction(Action actionToExecute){
    this.action = actionToExecute;
    action.execute();
    notifyObservers();
  }

  @Override
  public void add(Observer observer) {

  }

  @Override
  public void notifyObservers() {

  }
}
