package banking;

/**
 * Created by prayagupd
 * on 4/15/15.
 */

public interface AccountManager {
  public void add(Observer observer);
  public void notifyObservers();
}
