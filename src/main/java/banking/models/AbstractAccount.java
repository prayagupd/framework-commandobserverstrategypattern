package banking.models;

/**
 * Created by 984493
 * on 4/15/2015.
 */

public abstract class AbstractAccount {
  private String accountNumber;
  private double currentBalance;
  private double interestRate;

  public abstract void addInterest();
}
