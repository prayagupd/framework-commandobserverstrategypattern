package banking.models;

import banking.AccountManager;
import banking.Observer;

/**
 * Created by prayagupd on 4/15/15.
 */
public abstract class AbstractCustomer implements Observer{
  private AbstractAccount account;

  private String name;
  private String street;
  private String city;
  private String state;
  private String zipCode;
  private String email;

  public void addAccount(AbstractAccount account){

  }

  public void removeAccount(AbstractAccount account){

  }

  public void update(AccountManager accountManager){

  }
}
