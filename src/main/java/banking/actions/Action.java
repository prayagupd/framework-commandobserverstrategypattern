package banking.actions;

import banking.models.AbstractAccount;

/**
 * Created by 984493
 * on 4/15/2015.
 */

public interface Action {
    public void execute();
}
